var app = angular.module("dependentDropdown", ['ui.router']);

app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
   $stateProvider
   .state('home', {
      url: '/home',
      templateUrl: 'templates/main.html',
      controller: 'homeController'
   });
   $urlRouterProvider.otherwise('home');
}]);

app.controller("homeController", ['$scope', '$filter', function($scope, $filter){

   $scope.showInitial = false;

   $scope.details = {
      Company:'',
      Trainer: '',
      Trainee: ''
   };

   $scope.companies = [
      {"id": 1, "company": "Taj"},
      {"id": 2, "company": "Fortis"},
      {"id": 3, "company": "Oberoi"}
   ];

   var traineeList = [
      {"id": 1, "trainee": "Ankit", "companyId": 1},
      {"id": 2, "trainee": "Anurag", "companyId": 2},
      {"id": 3, "trainee": "Ajay", "companyId": 3},
      {"id": 4, "trainee": "Bimol", "companyId": 3},
      {"id": 5, "trainee": "Sanjay", "companyId": 2},
      {"id": 6, "trainee": "Ramesh", "companyId": 1}
   ];

   var trainerList = [
      {"id": 1, "trainer": "Nitin", "companyId": 1},
      {"id": 2, "trainer": "Puneet", "companyId": 1},
      {"id": 3, "trainer": "Sumit", "companyId": 2},
      {"id": 4, "trainer": "Karan", "companyId": 2},
      {"id": 5, "trainer": "Pranay", "companyId": 3}
   ];

   $scope.criteria = ['Practical', 'Written test', 'Viva', 'Roleplay'];

   $scope.getTraineeByCompany = function(){
      console.log($scope.details.Company);
      var trainee = $filter('filter')(traineeList, {companyId: $scope.details.Company});
      console.log(trainee);
      $scope.newTraineeList =  trainee;
   };

   $scope.getTrainerByCompany = function(){
      var trainer = $filter('filter')(trainerList, {companyId: $scope.details.Company});
      console.log(trainer);
      $scope.newTrainerList = trainer;
   };

   $scope.getAllData = function(){
      console.log($scope.details);
      $scope.newData = {
         Company: '',
         Trainer: '',
         Trainee: ''
      };
      $scope.newData.Company = $filter("filter")($scope.companies, {id: $scope.details.Company});

      if($scope.details.Trainer){
         $scope.newData.Trainer = $filter("filter")(trainerList, {id: $scope.details.Trainer});
      }
      else {
         $scope.newData.Trainer = $filter('filter')(trainerList, {companyId: $scope.details.Company});
      }


      if($scope.details.Trainee){
         $scope.newData.Trainee = $filter("filter")(traineeList, {id: $scope.details.Trainee});
      }
      else{
         $scope.newData.Trainee = $filter('filter')(traineeList, {companyId: $scope.details.Company});
      }

      $scope.showInitial = true;

      console.log($scope.newData);
      console.log($scope.newData.Trainee);
   }


   // --------------------------------------------------------------------
   $scope.criteria_index = 0;

   $scope.pass = function(criteria, id){
      console.log(criteria, id);
      if ($scope.criteria_index >= $scope.criteria.length - 1) {
         $scope.criteria_index = 0;
      } else {
         $scope.criteria_index++;
      }
      var trainee = $filter('filter')($scope.newTraineeList, {id: id});
      console.log(trainee);
      trainee[0][criteria] = 'pass';
   }

   $scope.fail = function(criteria, id){
      console.log(criteria, id);
      if ($scope.criteria_index >= $scope.criteria.length - 1) {
         $scope.criteria_index = 0;
      } else {
         $scope.criteria_index++;
      }
      var trainee = $filter('filter')($scope.newTraineeList, {id: id});
      console.log(trainee);
      trainee[0][criteria] = 'fail';
   }
   // --------------------------------------------------------------------
}]);
