var app = angular.module("dependentDropdown", ['ui.router']);

app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
   $stateProvider
   .state('home', {
      url: '/home',
      templateUrl: 'templates/main.html',
      controller: 'homeController'
   });
   $urlRouterProvider.otherwise('home');
}]);

app.controller("homeController", ['$scope', 'LocationService', function($scope, LocationService){

   console.log('testing');
   $scope.location = {
      Country:'',
      State: '',
      City: ''
   };

   $scope.countries = LocationService.getCountry();

   $scope.getStatesByCountry = function(){
      $scope.sates = LocationService.getCountryState($scope.location.Country);
      $scope.cities =[];
   };

   $scope.getCitiesByStates = function(){
      $scope.cities = LocationService.getStateCity($scope.location.State);
   };

   $scope.getAllData = function(){
      alert('success ' + JSON.stringify($scope.location));
   };
}]);

app.factory("LocationService", ['$filter', function($filter){

   var service = {};

   var countryList = [
      { "id": 1, "country": "USA" },
      { "id": 2, "country": "India" }
   ];

   var stateList = [
      {"id":1, "state":"Alaska", "countryId": 1},
      {"id":2, "state":"California", "countryId": 1},
      {"id":3, "state":"New York", "countryId": 1},
      {"id":4, "state":"Gujarat", "countryId": 2},
      {"id":5, "state":"Rajasthan", "countryId": 2},
      {"id":6, "state":"Maharashtra", "countryId": 2}
   ];

   var cityList = [
      {"id":1, "city":"Anchorage", "stateId": 1},
      {"id":2, "city":"Fairbanks", "stateId": 1},
      {"id":3, "city":"Lakes", "stateId": 1},
      {"id":7, "city":"Benicia", "stateId": 2},
      {"id":8, "city":"Clovis", "stateId": 2},
      {"id":9, "city":"Dublin", "stateId": 2},
      {"id":10, "city":"Manhattan", "stateId": 3},
      {"id":11, "city":"Bronx", "stateId": 3},
      {"id":12, "city":"Brooklyn", "stateId": 3},
      {"id":13, "city":"Queens", "stateId": 3},
      {"id":14 , "city": "Surat", "stateId": 4},
      {"id":15 , "city": "Baroda", "stateId": 4},
      {"id":16 , "city": "Rajkot", "stateId": 4},
      {"id":17 , "city": "Ajmer", "stateId": 5},
      {"id":18 , "city": "Jaipur", "stateId": 5},
      {"id":19 , "city": "Pune", "stateId": 6},
      {"id":20 , "city": "Mumbai", "stateId": 6}
   ];

   service.getCountry = function(){
      return countryList;
   };

   service.getCountryState = function(countryId){
      var states = $filter('filter')(stateList, {countryId: countryId});
      console.log(states);
      return states;
   };


   service.getStateCity = function(stateId){
      var items = $filter('filter')(cityList, {stateId: stateId});
      console.log(items);
      return items;
   };

   return service;


}]);
